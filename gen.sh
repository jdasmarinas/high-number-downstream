#!/bin/bash

rm -rf .gitlab-ci.yml

for i in $(seq 160); do
    cat << EOF > local/job-$i.yml
job-$i:
  script:
    - ls
EOF

    cat << EOF >> .gitlab-ci.yml
trigger_job-$i:
  trigger:
    include:
      - local: local/job-$i.yml
EOF
done
